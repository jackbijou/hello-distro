import os
import subprocess

work_folder = '~/churn'
script_folder = str(subprocess.run("pwd", capture_output=True).stdout.decode()).rstrip()


def execute():
    print(f"  Work Folder: {work_folder}")
    print(f"Script Folder: {script_folder}")
    os.system(f'mkdir {work_folder} && cd {work_folder}')
    run_script("install-signal.sh")
    run_script("install-brave.sh")
    run_script("install-mullvad.sh")
    #run_script("install-element.sh")
    run_script("install-sublime-merge.sh")
    #run_script("install-syncthing.sh")
    run_script("install-tech-tools.sh")
    run_script("install-favorites.sh")
    #run_script("install-docker.sh")
    run_script("update-bashrc.sh")

    #run_script("download-ssh-key-creator.sh")
    os.system(f'cd {script_folder} && rm -rf {work_folder}')


def run_script(script):
    print(f"Running script {script}")
    path = f"{script_folder}/{script}"
    os.system(f"chmod a+x {path} && sh {path}")


if __name__ == '__main__':
    execute()
