#!/bin/bash
sudo apt install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386 qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils git wget

sudo curl --create-dirs -L -o /etc/udev/rules.d/51-android.rules https://raw.githubusercontent.com/snowdream/51-android/master/51-android.rules
sudo chmod a+r /etc/udev/rules.d/51-android.rules
sudo service udev restart

sudo apt install cpu-checker
egrep -c '(vmx|svm)' /proc/cpuinfo

kvm-ok

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
