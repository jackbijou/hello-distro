#!/bin/bash
echo "neofetch
alias ls='ls -laF'
alias ll='ls -l'
alias upgrayedd='sudo apt update && sudo apt upgrade'
alias up='upgrayedd'" >> ~/.bashrc
source ~/.bashrc
