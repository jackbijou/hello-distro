#!/bin/bash
# This was adapted from instructions given on YouTube by LearnLinuxTV for debian systems:
# video: https://www.youtube.com/watch?v=J1bCWv14zYg
# wiki: https://wiki.learnlinux.tv/index.php/Syncing_your_Files_Across_ALL_your_Computers_via_Syncthing

# downloads a service file and enables syncthing to run in the back ground for the user that runs this

sudo apt install curl &&
curl -s https://syncthing.net/release-key.txt | sudo apt-key add - &&
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list &&
sudo apt update &&
sudo apt install syncthing &&
wget https://raw.githubusercontent.com/syncthing/syncthing/main/etc/linux-systemd/system/syncthing%40.service &&
sudo chown root: syncthing@.service &&
sudo mv syncthing@.service /etc/systemd/system &&
sudo systemctl daemon-reload &&
sudo systemctl enable syncthing@"$LOGNAME" &&
sudo systemctl start syncthing@"$LOGNAME"
